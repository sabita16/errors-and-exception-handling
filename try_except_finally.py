# Handle the exception thrown by the code below by using try and except blocks.
# Then use a finally keyword .

x = 40
y = 0
try:
    z = x/y
except:
    print("An error because cannot divide by zero!!")
finally:
    print("All done")

print(z)