# This is an example of a function that asks for an integer and prints the square of it.
# Using while loop with a try, except, else keyword for incorrect inputs.

def asking_int():
    while True:
        try:
            number = int(input('Please write an integer'))
        except:
            print('This is not an integer. Please try again')
            continue
        else:
            break
    print('Thank you. The result of your numbers square is:', number**2)

asking_int()